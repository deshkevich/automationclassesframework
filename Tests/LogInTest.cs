﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using AutomationClassesFramework.Pages;
using AutomationClassesFramework.Utils;

namespace AutomationClassesFramework.Tests
{
    [TestFixture]
    [Parallelizable]
    class LogInTest
    {
        MainPage mainPage;
        private const string CORRECT_EMAIL = "dao.is.mao@gmail.com";
        private const string CORRECT_PASSWORD = "qwerty12345";

        [SetUp]
        public void OpenMainPage()
        {
            mainPage = new MainPage();
        }

        [Test]
        public void LogInPossitive()
        {
            bool isLogInSuccessful = mainPage.LogIn(CORRECT_EMAIL, CORRECT_PASSWORD, false);
            Assert.True(isLogInSuccessful);
            mainPage.LogOut();
        }

        [TestCase("NegativeCase1")]
        [TestCase("NegativeCase2")]
        public void LogInNegative(string testCase)
        {
            var userData = ExcelDataAccess.GetTestData(testCase);
            bool isIncorrectCredentialsMessageAppear = mainPage.LogIn(userData.Username, userData.Password, true);
            Assert.True(isIncorrectCredentialsMessageAppear,
                string.Format("Error message not appear with incorrect credentials {0} - {1}", userData.Username, userData.Password));
        }

        [TearDown]
        public void CloseDriver()
        {
            mainPage.CloseBrowser();
        }
    }
}

