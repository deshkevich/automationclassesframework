﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Extensions;
using OpenQA.Selenium.Support.Events;
using System.Drawing.Imaging;

namespace AutomationClassesFramework.Pages
{
    public class MainPage : BasePage
    {
        private string url = "https://www.kinopoisk.ru/";

        private const string LOGIN_BUTTON_XPATH = "//a[text()='Войти на сайт']";
        private const string AUTH_FRAME_XPATH = "//iframe[@name='kp2-authapi-iframe']";
        private const string NAME_FIELD_XPATH = "//input[@name='login']";
        private const string PASSWORD_FIELD_XPATH = "//input[@name='password']";
        private const string ENTER_BUTTON_XPATH = "//span[text()='Вход']";
        private const string FAILURE_TEXT_XPATH = "//div[text()='Вы ошиблись в почте или пароле']";
        private const string LOGOUT_BUTTON_XPATH = "//a[@href='/logout/']";

        public MainPage()
        {
            driver.Url = url;
        }

        public bool LogIn(string username, string password, bool checkFailure)
        {
            driver.FindElement(By.XPath(LOGIN_BUTTON_XPATH)).Click();
            fillEmailField(username);
            driver.FindElement(By.XPath(PASSWORD_FIELD_XPATH)).SendKeys(password);
            IWebElement enterButton = driver.FindElement(By.XPath(ENTER_BUTTON_XPATH));

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
            wait.Until(ExpectedConditions.ElementToBeClickable(enterButton)); // ExpectedConditions требует пакета Selenium.Support

            enterButton.Click();
            if(checkFailure)
                return driver.FindElement(By.XPath(FAILURE_TEXT_XPATH)).Displayed;
            else
                return driver.FindElement(By.XPath(LOGOUT_BUTTON_XPATH)).Displayed;
        }

        public bool LogOut()
        {
            driver.FindElement(By.XPath(LOGOUT_BUTTON_XPATH)).Click();
            return driver.FindElement(By.XPath(LOGIN_BUTTON_XPATH)).Displayed;
        }

        public MainPage fillEmailField(string username)
        {
            driver.SwitchTo().Frame(driver.FindElement(By.XPath(AUTH_FRAME_XPATH)));
            driver.FindElement(By.XPath(NAME_FIELD_XPATH)).SendKeys(username);
            return this;
        }

        public string getEmailField()
        {
            return driver.FindElement(By.XPath(NAME_FIELD_XPATH)).Text;
        }
    }
}
