﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using AutomationClassesFramework.Utils;

namespace AutomationClassesFramework.Pages
{
    public class BasePage
    {
        protected IWebDriver driver;

        public BasePage()
        {
            driver = WebDriverRunner.GetDriver();
        }

        public void CloseBrowser()
        {
            driver.Quit();
        }
    }
}
