﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Engine;
using NUnit.Engine.Extensibility;
using NLog;


namespace AutomationClassesFramework.Utils
{
    [Extension(Description = "Test Reporter Extension", EngineVersion= "3.4")]
    class UnitTestsListener : ITestEventListener
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public void OnTestEvent(string report)
        {
            WriteNUnitEventToLog(report);
        }

        private void WriteNUnitEventToLog(string report)
        {
            logger.Debug(report);
        }
    }
}
