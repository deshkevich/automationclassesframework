﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NLog;

namespace AutomationClassesFramework.Utils
{
    class WebDriverRunner
    {
        private static IWebDriver driver;
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static int defaulTimeoutSeconds = 10;

        private WebDriverRunner()
        {
            WebDriverFactory factory = new WebDriverFactory();
            logger.Debug("Создаем сущность webdriver");
            driver = factory.CreateDriver(Config.BrowserType);
            logger.Debug("Устанавливаем время ожидания {0} секунд", defaulTimeoutSeconds);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(defaulTimeoutSeconds);
        }

        public static IWebDriver GetDriver()
        {
            new WebDriverRunner();
            return driver;
        }
    }
}

