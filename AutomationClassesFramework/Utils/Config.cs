﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace AutomationClassesFramework.Utils
{
    class Config
    {
        public static string BrowserType
        {
            get
            {
                return ConfigurationManager.AppSettings["BrowserType"];
            }
        }

        public static string TestDataSheetPath
        {
            get
            {
                return ConfigurationManager.AppSettings["TestDataSheetPath"];
            }
        }
    }
}
