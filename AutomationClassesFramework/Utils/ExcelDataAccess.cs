﻿using System.Configuration;
using System.Data.OleDb;
using System.Linq;
using Dapper;
using System.IO;
using System;

namespace AutomationClassesFramework.Utils
{
    public class ExcelDataAccess
    {
        public static string TestDataFileConnection()
        {
            var fileName = ConfigurationManager.AppSettings["TestDataSheetPath"];
            string wanted_path = Path.Combine(Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent.Parent.FullName, fileName);
            var con = string.Format(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source = {0}; Extended Properties=Excel 12.0;", wanted_path);
            return con;
        }

        public static TestCaseData GetTestData(string keyName)
        {
            using (var connection = new OleDbConnection(TestDataFileConnection()))
            {
                connection.Open();
                var query = string.Format("select * from [LogInDataset$] where key='{0}'", keyName);
                var value = connection.Query<TestCaseData>(query).FirstOrDefault();
                connection.Close();
                return value;
            }
        }
    }
}