﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.Events;
using OpenQA.Selenium.Support.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationClassesFramework.Utils
{
    class WebDriverFactory
    {
        IWebDriver driver;

        public IWebDriver CreateDriver(string browserType)
        {
            EventFiringWebDriver firingDriver = null;
            switch (browserType)
            {
                case "Chrome":
                    firingDriver = new EventFiringWebDriver(new ChromeDriver());
                    break;
                case "Firefox":
                    firingDriver = new EventFiringWebDriver(new FirefoxDriver());
                    break;
                default:
                    break;

            }
            firingDriver.ExceptionThrown += firingDriver_TakeScreenshotOnException;
            driver = firingDriver;
            return driver;

        }

        public void firingDriver_TakeScreenshotOnException(object sender, WebDriverExceptionEventArgs e)
        {
            string timestamp = DateTime.Now.ToString("yyyy-MM-dd-hhmm-ss");
            driver.TakeScreenshot().SaveAsFile("Exception-" + timestamp + ".png", ScreenshotImageFormat.Png);
        }
    }
}
